﻿using UnityEngine;
using System.Collections;

public class S_InputManager : MonoBehaviour {

	GameObject Hero;
	GameObject PathTracking;
	bool pressed = false;

	Vector3 contPosition;

	public AnimationCurve MovingSpeed;

	// Use this for initialization
	void Start () {

		Hero = GameObject.Find( "Hero" );
		PathTracking = GameObject.Find( "BonePathTracker" );

		contPosition = Hero.transform.localPosition;
	
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetKeyDown(KeyCode.LeftArrow) )
		{
			Hero.transform.localScale = new Vector3( 0.015f, 0.015f, 1f );
			Hero.animation.CrossFade( "AnimationHeroWalk" );

		} else
		if( Input.GetKey(KeyCode.LeftArrow) )
		{
			if( !Hero.animation.IsPlaying( "AnimationHeroWalk" ) )
			{
				contPosition = Hero.transform.localPosition;
				Hero.animation.Play("AnimationHeroWalk");
			} else
			{
				if( Hero.transform.localPosition.x > -67 ) 
					Hero.transform.localPosition = contPosition + new Vector3( PathTracking.transform.localPosition.x*Hero.transform.localScale.x, 0f, 0f );
				else
				{
					Hero.transform.localPosition = new Vector3 ( -67f, -21.27047f, 115.0203f );
					contPosition = Hero.transform.localPosition;
				}
			}
		} else
		if( Input.GetKeyUp(KeyCode.LeftArrow) )
		{
			contPosition = Hero.transform.localPosition;
			Hero.animation.CrossFade( "AnimationHeroIdle" );
		}

		if( Input.GetKeyDown(KeyCode.RightArrow) )
		{
			Hero.transform.localScale = new Vector3( -0.015f, 0.015f, 1f );
			Hero.animation.CrossFade( "AnimationHeroWalk" );
			
		} else
		if( Input.GetKey(KeyCode.RightArrow) )
		{
			if( !Hero.animation.IsPlaying( "AnimationHeroWalk" ) )
			{
				contPosition = Hero.transform.localPosition;
				Hero.animation.Play("AnimationHeroWalk");
			} else
			{
				if( Hero.transform.localPosition.x < 67 ) 
					Hero.transform.localPosition = contPosition + new Vector3( PathTracking.transform.localPosition.x*Hero.transform.localScale.x, 0f, 0f );
				else
				{
					Hero.transform.localPosition = new Vector3 ( 67f, -21.27047f, 115.0203f );
					contPosition = Hero.transform.localPosition;
				}
			}
		} else
		if( Input.GetKeyUp(KeyCode.RightArrow) )
		{
			contPosition = Hero.transform.localPosition;
			Hero.animation.CrossFade( "AnimationHeroIdle" );
		}
	}
}
