using System;
using UnityEngine;

public class Scripts_AFloor : MonoBehaviour
{
    private bool newlyCatched = false;
    public bool NewlyCathed
    {
        get
        {
            if (this.newlyCatched)
            {
                this.newlyCatched = false;
                return true;
            }
            return false;
        }
        set
        {
            this.newlyCatched = value;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        this.newlyCatched = true;
        GameObject.Find("BlobMiss").audio.Play();
    }
}


