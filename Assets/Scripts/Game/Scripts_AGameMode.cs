﻿using UnityEngine;
using System.Collections;

public class Scripts_AGameMode : MonoBehaviour {

	public Scripts_AGameState GameState;
	public Scripts_APlayerController PlayerController;
	public Scripts_AHud HUD;

	public Scripts_ACouple Couple;
	public Scripts_AFloor Floor;
	public Scripts_ABarrel[] Barrels;

	private float AwaitTime = 3.5f;
	private float DifficultMul = 0.992f;

	// Use this for initialization
	IEnumerator Start () {
		HUD.OnLeftPressed += PlayerController.OnLeftPressed;
		HUD.OnLeftReleased += PlayerController.OnLeftReleased;
		HUD.OnRightPressed += PlayerController.OnRightPressed;
		HUD.OnRightReleased += PlayerController.OnRightReleased;

		PlayerController.GameState = GameState;
		HUD.GameState = GameState;

		GameState.IngameState = Scripts_EIngameState.Playing;

		Random.seed = System.DateTime.Now.Millisecond;

		for (;;) 
		{
			if( GameState.IngameState == Scripts_EIngameState.Playing )
			{
				int index = 0;

				do
				{
					index = Random.Range(0, Barrels.Length);
				} 
				while( Barrels[index].animation.IsPlaying( "AnimationBarrelShot" ) );

				Barrels[index].animation.Play ("AnimationBarrelShot", PlayMode.StopAll );
			}

			yield return new WaitForSeconds( AwaitTime );

			AwaitTime *= DifficultMul;
		}

	}
	
	// Update is called once per frame
	void Update () {
		if( GameState.IngameState == Scripts_EIngameState.Playing )
		{
			if (Couple.NewlyCathed)
			{
				GameState.Score += 200;

			}

			HUD.ScoreText = GameState.Score.ToString ();

			if (Floor.NewlyCathed)
			{
				GameState.Health -= 1;

			}

			HUD.HealthCount = GameState.Health;

			if( GameState.Health == 0 )
			{
				GameState.IngameState = Scripts_EIngameState.Finished;
				GameObject.Find("Fon").audio.Stop();
				GameObject.Find("GameOver").audio.Play();

				StartCoroutine( "GameOver" );
			}
		}
	}

	IEnumerator GameOver()
	{
		HUD.CurrentScreen = "";

		yield return new WaitForSeconds( 4.0f );

		HUD.CurrentScreen = "Screen3";
	}
}
