﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using UnityEngine;

public class ScoreRecordComparer : IComparer
{
    int IComparer.Compare(object x, object y)
    {
        ScoreRecord record = (ScoreRecord) y;
        ScoreRecord record2 = (ScoreRecord) x;
		return (int) ( record.Score - record2.Score );
    }
}

public struct ScoreRecord
{
    public uint Score;
    public string PlayerName;
    public string Time;
}

public static class EncryptorDecryptor
{
    public static int key = 0xdb;

    public static string EncryptDecrypt(string textToEncrypt)
    {
        StringBuilder builder = new StringBuilder(textToEncrypt);
        StringBuilder builder2 = new StringBuilder(textToEncrypt.Length);
        for (int i = 0; i < textToEncrypt.Length; i++)
        {
            char ch = builder[i];
            ch = (char) (ch ^ key);
            builder2.Append(ch);
        }
        return builder2.ToString();
    }
}

public class Scripts_AHud : MonoBehaviour
{
    public string CurrentScreen;
    public GUIStyle FieldStyle;
    public Scripts_AGameState GameState;
    public uint HealthCount;
    public Texture HealthTexture;
    private bool isShareProcessing;
    public ArrayList Layers;
    private bool LeftContinuousPressedFlag;
    private bool LeftContinuousReleasedFlag = true;
    private float PSDHeight = 1600f;
    private float PSDWidth = 2560f;
    public GUIStyle RecordStyle;
    private bool RightContinuousPressedFlag;
    private bool RightContinuousReleasedFlag = true;
    private ArrayList ScoreRecords;
    public GUIStyle ScoreStyle;
    public string ScoreText;
    public TextAsset UIXML;

    public delegate void VoidEvent0(bool Continuous);

    public event VoidEvent0 OnLeftPressed;

    public event VoidEvent0 OnLeftReleased;

    public event VoidEvent0 OnRightPressed;

    public event VoidEvent0 OnRightReleased;

    public PSDLayer GetLayer(string Group, string Name)
    {
		for( int i = 0; i < Layers.Count; ++i )
		{
	        PSDLayer current = (PSDLayer) Layers[i];
	        if ((current.Group == Group) && (current.Name == Name))
	        {
	            return current;
	        }
        }

        return null;
    }

    private void OnGUI()
    {
        float dx = ((float) Screen.width) / PSDWidth;
        float dy = ((float) Screen.height) / PSDHeight;
    
        if (GameState.IngameState == Scripts_EIngameState.Finished)
        {
            PSDLayer layer = GetLayer("Screen8", "ImageOverlay");
            GUI.Box(new Rect(layer.X * dx, layer.Y * dy, layer.Width * dx, layer.Height * dy), string.Empty, layer.style);

            layer = GetLayer("Screen8", "ImageDeadman");
            GUI.Box(new Rect(layer.X * dx, layer.Y * dy, layer.Width * dx, layer.Height * dy), string.Empty, layer.style);

            layer = GetLayer("Screen8", "ImageGameOver");
            GUI.Box(new Rect(layer.X * dx, layer.Y * dy, layer.Width * dx, layer.Height * dy), string.Empty, layer.style);
        }

        if (CurrentScreen == "Screen2")
        {
            for (int i = 0; i < HealthCount; i++)
            {
                GUI.DrawTexture(new Rect(Screen.width * ((0.16f * i) + 0.06f), Screen.height * 0.08f, Screen.width * 0.1f, Screen.height * 0.1f), HealthTexture);
            }

            ScoreStyle.fontSize = (int) (((float) Screen.height) / 7f);
            GUI.Label(new Rect(Screen.width * 0.25f, Screen.height - (Screen.height * 0.28f), Screen.width * 0.5f, Screen.height * 0.25f), ScoreText, ScoreStyle);


            PSDLayer layer2 = GetLayer("Screen2", "ButtonCloseNormal");
            if (GUI.Button(new Rect(layer2.X * dx, layer2.Y * dy, layer2.Width * dx, layer2.Height * dy), string.Empty, layer2.style))
            {
                Application.LoadLevel("Ex_050814");
            }

            layer2 = GetLayer("Screen2", "ButtonLeftNormal");
            if (GUI.RepeatButton(new Rect(layer2.X * dx, layer2.Y * dy, layer2.Width * dx, layer2.Height * dy), string.Empty, layer2.style) && Input.GetMouseButton(0))
            {
                LeftContinuousReleasedFlag = false;
                OnLeftPressed(LeftContinuousPressedFlag);
                LeftContinuousPressedFlag = true;
            }
            else if (!Input.GetMouseButton(0))
            {
                LeftContinuousPressedFlag = false;
                OnLeftReleased(LeftContinuousReleasedFlag);
                LeftContinuousReleasedFlag = true;
            }

            layer2 = GetLayer("Screen2", "ButtonRightNormal");
            if (GUI.RepeatButton(new Rect(layer2.X * dx, layer2.Y * dy, layer2.Width * dx, layer2.Height * dy), string.Empty, layer2.style) && Input.GetMouseButton(0))
            {
                RightContinuousReleasedFlag = false;
                OnRightPressed(RightContinuousPressedFlag);
                RightContinuousPressedFlag = true;
            }
            else if (!Input.GetMouseButton(0))
            {
                RightContinuousPressedFlag = false;
                OnRightReleased(RightContinuousReleasedFlag);
                RightContinuousReleasedFlag = true;
            }
        }
        else 
        if (CurrentScreen == "Screen3")
        {
            PSDLayer layer3 = GetLayer("Screen3", "ImageBoard");
            GUI.Box(new Rect(layer3.X * dx, layer3.Y * dy, layer3.Width * dx, layer3.Height * dy), string.Empty, layer3.style);

            ScoreStyle.fontSize = (int) (((float) Screen.height) / 8.5f);
            GUI.Label(new Rect(Screen.width * 0.25f, Screen.height * 0.66f, Screen.width * 0.2f, Screen.height * 0.1f), ScoreText, ScoreStyle);
            
            ScoreStyle.normal.textColor = Color.red;
            GUI.Label(new Rect(Screen.width * 0.53f, Screen.height * 0.66f, Screen.width * 0.2f, Screen.height * 0.1f), (ScoreRecords.Count <= 0) ? "---": ((ScoreRecord) ScoreRecords[0]).Score.ToString(), ScoreStyle);
           
            ScoreStyle.fontSize = (int) (((float) Screen.height) / 7f);
            ScoreStyle.normal.textColor = Color.white;
           
            layer3 = GetLayer("Screen3", "ButtonRecordsNormal");
            if (GUI.Button(new Rect(layer3.X * dx, layer3.Y * dy, layer3.Width * dx, layer3.Height * dy), string.Empty, layer3.style))
            {
                CurrentScreen = "Screen5";
            }

            layer3 = GetLayer("Screen3", "ButtonSaveNormal");
            if (GUI.Button(new Rect(layer3.X * dx, layer3.Y * dy, layer3.Width * dx, layer3.Height * dy), string.Empty, layer3.style))
            {
                CurrentScreen = "Screen4";
            }

            layer3 = GetLayer("Screen3", "ButtonRestartNormal");
            if (GUI.Button(new Rect(layer3.X * dx, layer3.Y * dy, layer3.Width * dx, layer3.Height * dy), string.Empty, layer3.style))
            {
                Application.LoadLevel("Game");
            }

            layer3 = GetLayer("Screen1", "ButtonCloseNormal");
            if (GUI.Button(new Rect(layer3.X * dx, layer3.Y * dy, layer3.Width * dx, layer3.Height * dy), string.Empty, layer3.style))
            {
                Application.LoadLevel("Ex_050814");
            }
        }
        else if (CurrentScreen == "Screen4")
        {
            PSDLayer layer4 = GetLayer("Screen4", "ImageBoard");
            GUI.Box(new Rect(layer4.X * dx, layer4.Y * dy, layer4.Width * dx, layer4.Height * dy), string.Empty, layer4.style);

            layer4 = GetLayer("Screen4", "ImagePaper");
            GUI.Box(new Rect(layer4.X * dx, layer4.Y * dy, layer4.Width * dx, layer4.Height * dy), string.Empty, layer4.style);

            GUI.skin.settings.cursorColor = Color.black;
            GUI.skin.settings.doubleClickSelectsWord = false;
            GUI.skin.settings.tripleClickSelectsLine = false;

            FieldStyle.fontSize = (int) (((float) Screen.height) / 16f);
            GameState.PlayerName = Regex.Replace( GUI.TextField(new Rect(layer4.X * dx, layer4.Y * dy, layer4.Width * dx, layer4.Height * dy), GameState.PlayerName, 20, FieldStyle), "[^a-zA-Zа-яА-Я]", string.Empty);
          
            layer4 = this.GetLayer("Screen4", "ImageInputHint");
            GUI.Box(new Rect(layer4.X * dx, layer4.Y * dy, layer4.Width * dx, layer4.Height * dy), string.Empty, layer4.style);
         
            layer4 = GetLayer("Screen4", "ButtonSaveNormal");
            if (GUI.Button(new Rect(layer4.X * dx, layer4.Y * dy, layer4.Width * dx, layer4.Height * dy), string.Empty, layer4.style))
            {
                PlayerPrefs.SetString("PlayerName", GameState.PlayerName);
                PlayerPrefs.Save();
                CurrentScreen = "Screen5";
                StartCoroutine("SaveScore");
            }

            layer4 = GetLayer("Screen4", "ButtonCloseNormal");
            if (GUI.Button(new Rect(layer4.X * dx, layer4.Y * dy, layer4.Width * dx, layer4.Height * dy), string.Empty, layer4.style))
            {
                CurrentScreen = "Screen3";
            }
        }
        else if (CurrentScreen == "Screen5")
        {
            PSDLayer layer5 = GetLayer("Screen5", "ImageBoard");
            GUI.Box(new Rect(layer5.X * dx, layer5.Y * dy, layer5.Width * dx, layer5.Height * dy), string.Empty, layer5.style);

            layer5 = GetLayer("Screen5", "ImageTable");
            GUI.Box(new Rect(layer5.X * dx, layer5.Y * dy, layer5.Width * dx, layer5.Height * dy), string.Empty, layer5.style);

            layer5 = GetLayer("Screen5", "ImagePaper");
            GUI.Box(new Rect(layer5.X * dx, layer5.Y * dy, layer5.Width * dx, layer5.Height * dy), string.Empty, layer5.style);

            RecordStyle.fontSize = (int) (((float) Screen.height) / 25f);
            if (ScoreRecords.Count > 0)
            {
                for (int j = 0; (j < ScoreRecords.Count) && (j < 11); j++)
                {
                    switch (j)
                    {
                        case 0:
                            RecordStyle.normal.textColor = Color.red;
                            RecordStyle.fontSize = (int) (((float) Screen.height) / 20f);
                            break;

                        case 1:
                            RecordStyle.normal.textColor = Color.red;
                            RecordStyle.fontSize = (int) (((float) Screen.height) / 22f);
                            break;

                        case 2:
                            RecordStyle.normal.textColor = Color.red;
                            RecordStyle.fontSize = (int) (((float) Screen.height) / 23f);
                            break;

                        default:
                            RecordStyle.normal.textColor = Color.black;
                            RecordStyle.fontSize = (int) (((float) Screen.height) / 25f);
                            break;
                    }

                    ScoreRecord record = (ScoreRecord) ScoreRecords[j];
                    GUI.Label(new Rect(338f * dx, ((380 + (0x4b * j)) + 10) * dy, 78f * dx, 55f * dy), (j + 1).ToString(), RecordStyle);
                    GUI.Label(new Rect(444f * dx, ((380 + (0x4b * j)) + 10) * dy, 795f * dx, 55f * dy), record.PlayerName, RecordStyle);
                    GUI.Label(new Rect(1267f * dx, ((380 + (0x4b * j)) + 10) * dy, 540f * dx, 55f * dy), record.Score.ToString(), RecordStyle);
                    RecordStyle.fontSize = (int) (((float) Screen.height) / 40f);
                    GUI.Label(new Rect(1821f * dx, ((380 + (0x4b * j)) + 10) * dy, 373f * dx, 55f * dy), record.Time, RecordStyle);
                    RecordStyle.fontSize = (int) (((float) Screen.height) / 25f);
                }
            }

            layer5 = GetLayer("Screen5", "ButtonCloseNormal");
            if (GUI.Button(new Rect(layer5.X * dx, layer5.Y * dy, layer5.Width * dx, layer5.Height * dy), string.Empty, layer5.style))
            {
                CurrentScreen = "Screen3";
            }

            layer5 = GetLayer("Screen5", "ButtonShareNormal");
            if (GUI.Button(new Rect(layer5.X * dx, layer5.Y * dy, layer5.Width * dx, layer5.Height * dy), string.Empty, layer5.style))
            {
                StartCoroutine("ShareScreenshot");
            }
        }
    }

    IEnumerator LoadScores()
    {
        ScoreRecords.Clear();
        string path = Path.Combine(Application.persistentDataPath, "saves.dat");
        StreamReader sr = File.OpenText( path );
        while (!sr.EndOfStream)
        {
            string[] values = EncryptorDecryptor.EncryptDecrypt( sr.ReadLine()).Split(',');
            
            ScoreRecord record = new ScoreRecord();
            record.Score = (uint) Convert.ToInt32( values[0] );
            record.PlayerName = values[1];
            record.Time = values[2];
            ScoreRecords.Add( record );
        }

        ScoreRecords.Sort(new ScoreRecordComparer());

		yield return null; 
    }

    public IEnumerator SaveScore()
    {
        string path = Path.Combine(Application.persistentDataPath, "saves.dat");
        string text = EncryptorDecryptor.EncryptDecrypt(GameState.Score.ToString() + "," + GameState.PlayerName + "," + DateTime.Now.ToString("dd.MM.yy HH:mm")) + Environment.NewLine;

        File.AppendAllText(path, text);
        StartCoroutine("LoadScores");

        yield return null;  
    }

    public IEnumerator ShareScreenshot()
    {
        isShareProcessing = true;
        
        yield return new WaitForEndOfFrame();

        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        screenTexture.ReadPixels( new Rect(0f, 0f, (float) Screen.width, (float) Screen.height), 0, 0);
        screenTexture.Apply();
                    
        byte[] dataToSave = screenTexture.EncodeToPNG();

        string destination = Path.Combine(Application.persistentDataPath, DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");

        File.WriteAllBytes( destination, dataToSave );
        
        if (!Application.isEditor)
        {
			// block to open the file and share it ------------START
			AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
			AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
			intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
			AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
			AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","file://" + destination);
			intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
			//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "testo");
			//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");
			intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
			AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
			
			// option one:
			currentActivity.Call("startActivity", intentObject);
        }
        
        isShareProcessing = false;
    }

    private void Start()
    {
        if ((((float) Screen.height) / ((float) Screen.width)) < 0.625f)
        {
            camera.orthographicSize = (1280f * Screen.height) / ((float) Screen.width);
        }
        ScoreStyle.fontSize = (int) (((float) Screen.height) / 7f);
        
        Layers = new ArrayList();
        ScoreRecords = new ArrayList();
        StartCoroutine("LoadScores");
        
		XmlDocument document = new XmlDocument();
		document.LoadXml(UIXML.text);
		XmlNodeList nodes = document.SelectNodes ("psd");
		
		for( int i = 0; i < nodes.Count; ++i )
		{
			XmlNode current = (XmlNode) nodes[i];
			
			PSDWidth = Convert.ToInt32(current.Attributes["width"].Value);
			PSDHeight = Convert.ToInt32(current.Attributes["height"].Value);
			
			XmlNodeList childs = current.ChildNodes;
			
			for( int j = 0; j < childs.Count; ++j )
			{
				XmlNode node2 = (XmlNode) childs[j];
				string[] strArray = node2.Attributes["position"].Value.Split(',');
				PSDLayer layer = new PSDLayer(
					node2.Attributes["group"].Value, 
					node2.Attributes["name"].Value, 
					Convert.ToInt32(node2.Attributes["stack"].Value), 
					Convert.ToInt32(strArray[0]), 
					Convert.ToInt32(strArray[1]), 
					Convert.ToInt32(node2.Attributes["layerwidth"].Value), 
					Convert.ToInt32(node2.Attributes["layerheight"].Value), 
					node2.InnerText, 
					Resources.Load(node2.InnerText.Split('.')[0]) as Texture2D
					);
				Layers.Add(layer);
			}
		}

        for (int i = 0; i < Layers.Count; ++i)
        {
            PSDLayer layer2 = Layers[i] as PSDLayer;

            if (layer2.Name.Contains("Button") && layer2.Name.Contains("Normal"))
            {
                layer2.style.active.background = ((PSDLayer) Layers[i - 1]).style.normal.background;
            }
        }
    }

    private void Update()
    {
    }
}
