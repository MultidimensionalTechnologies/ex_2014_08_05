using System;
using UnityEngine;

public class Scripts_ACouple : MonoBehaviour
{
    private bool newlyCatched = false;
    public bool NewlyCathed
    {
        get
        {
            if (newlyCatched)
            {
                newlyCatched = false;
                return true;
            }
            return false;
        }
        set
        {
            newlyCatched = value;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        other.gameObject.transform.parent.parent.parent.gameObject.animation.Play("AnimationBarrelIdle", PlayMode.StopAll);
        newlyCatched = true;

        GameObject.Find("Catched").audio.Play();
    }
}
