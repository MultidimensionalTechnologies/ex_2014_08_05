using System;
using System.Collections;
using UnityEngine;

public class Scripts_APlayerController : MonoBehaviour
{
    private Vector3 ConstantPosition;
    public Scripts_AGameState GameState;
    public GameObject PathTracking;
    public GameObject PlayerCharacter;
    public GameObject StepSound;

    public void OnLeftPressed(bool Continuous)
    {
        if ( GameState.IngameState == Scripts_EIngameState.Playing )
        {
            if (Continuous)
            {
                if (!PlayerCharacter.animation.IsPlaying("AnimationHeroWalk"))
                {
                    ConstantPosition = PlayerCharacter.transform.localPosition;
                    PlayerCharacter.animation.Play("AnimationHeroWalk");
                    StartCoroutine("PlayStepSound");
                }
                else 
                if (PlayerCharacter.transform.localPosition.x > -1280f)
                {
                    PlayerCharacter.transform.localPosition = ConstantPosition + new Vector3(PathTracking.transform.localPosition.x * PlayerCharacter.transform.localScale.x, 0f, 0f);
                }
            }
            else
            {
                PlayerCharacter.transform.localScale = new Vector3(1f, 1f, 1f);
                ConstantPosition = PlayerCharacter.transform.localPosition;
                PlayerCharacter.animation.CrossFade("AnimationHeroWalk");
                StartCoroutine("PlayStepSound");
            }
        }
    }

    public void OnLeftReleased(bool Continuous)
    {
        if ((GameState.IngameState == Scripts_EIngameState.Playing) && !Continuous)
        {
            ConstantPosition = PlayerCharacter.transform.localPosition;
            PlayerCharacter.animation.CrossFade("AnimationHeroIdle");
        }
    }

    public void OnRightPressed(bool Continuous)
    {
        if (GameState.IngameState == Scripts_EIngameState.Playing)
        {
            if (Continuous)
            {
                if (!PlayerCharacter.animation.IsPlaying("AnimationHeroWalk"))
                {
                    ConstantPosition = PlayerCharacter.transform.localPosition;
                    PlayerCharacter.animation.Play("AnimationHeroWalk");
                    StartCoroutine("PlayStepSound");
                }
                else 
                if (PlayerCharacter.transform.localPosition.x < 1280f)
                {
                    PlayerCharacter.transform.localPosition = ConstantPosition + new Vector3(PathTracking.transform.localPosition.x * PlayerCharacter.transform.localScale.x, 0f, 0f);
                }
            }
            else
            {
                PlayerCharacter.transform.localScale = new Vector3(-1f, 1f, 1f);
                ConstantPosition = PlayerCharacter.transform.localPosition;
                PlayerCharacter.animation.CrossFade("AnimationHeroWalk");
                StartCoroutine("PlayStepSound");
            }
        }
    }

    public void OnRightReleased(bool Continuous)
    {
        if ((GameState.IngameState == Scripts_EIngameState.Playing) && !Continuous)
        {
            ConstantPosition = PlayerCharacter.transform.localPosition;
            PlayerCharacter.animation.CrossFade("AnimationHeroIdle");
        }
    }

    private IEnumerator PlayStepSound()
    {
        StepSound.audio.Play();
        yield return null;
    }

    private void Start()
    {
        PlayerCharacter = GameObject.Find("PlayerCharacter");
        PathTracking = GameObject.Find("BonePathTracker");
        ConstantPosition = PlayerCharacter.transform.localPosition;
    }
}
