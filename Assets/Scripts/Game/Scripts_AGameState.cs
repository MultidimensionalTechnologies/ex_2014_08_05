using System;
using UnityEngine;

public enum Scripts_EIngameState
{
    None = 0,
    Playing = 1,
    Finished = 2
}


public class Scripts_AGameState : MonoBehaviour
{
    public uint Health = 6;
    public Scripts_EIngameState IngameState = Scripts_EIngameState.None;
    public string PlayerName = "Player";
    public uint Score = 0;

    private void Start()
    {
        this.PlayerName = PlayerPrefs.GetString("PlayerName");
        this.PlayerName = this.PlayerName == string.Empty ? "Player" : this.PlayerName;
    }
}
