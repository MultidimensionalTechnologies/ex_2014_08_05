using System;
using UnityEngine;

public class S_Bookslider : MonoBehaviour
{
    public uint CurrentPage;
    public S_UserInterface UI;

    private string GetAnimByNum(uint Index)
    {
        switch (Index)
        {
            case 0:
                return "Header";

            case 1:
                return "Page 1";

            case 2:
                return "Page 2";

            case 3:
                return "Page 3";

            case 4:
                return "Page 4";
        }

        return string.Empty;
    }

    private void Start()
    {
        UI = Camera.main.GetComponent<S_UserInterface>();
    }

    private void Update()
    {
        if (((UI.CurrentScreen == "Screen1") && Input.GetMouseButtonDown(0)) && !animation.isPlaying)
        {
            RaycastHit[] hitArray = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition));
            for (int i = 0; i < hitArray.Length; i++)
            {
                if (hitArray[i].collider.gameObject.name == "ButtonLeft")
                {
                    if (CurrentPage > 0)
                    {
                        animation[GetAnimByNum(CurrentPage - 1)].speed = -1f;
                        animation[GetAnimByNum(CurrentPage - 1)].time = animation[GetAnimByNum(CurrentPage - 1)].length;
                        animation.Play(GetAnimByNum(CurrentPage - 1));
                        CurrentPage--;
                    }
                }
                else if (hitArray[i].collider.gameObject.name == "ButtonRight")
                {
                    if (CurrentPage < 5)
                    {
                        animation[GetAnimByNum(CurrentPage)].speed = 1f;
                        animation[GetAnimByNum(CurrentPage)].time = 0f;
                        animation.Play(GetAnimByNum(CurrentPage));
                        CurrentPage++;
                    }
                }
                else if ((hitArray[i].collider.gameObject.name == "ButtonOpen") && (CurrentPage > 1))
                {
                    UI.ShowRecipe(CurrentPage - 2);
                }
            }
        }
    }
}
