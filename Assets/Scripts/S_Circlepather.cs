﻿using UnityEngine;
using System.Collections;

public class S_Circlepather : MonoBehaviour {
		
	public float RadiusX = 40f;
	public float RadiusY = 40f;
	public float Speed = 1.0f;
	public float DeltaView = 0.1f;
	public float OffsetTime = 0.0f;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.time * Speed + OffsetTime;
		float lx = Mathf.Sin (t) * RadiusX;
		float ly = Mathf.Cos (t) * RadiusY;

		transform.localPosition = new Vector3 (lx, 0f, ly);
	}
}
