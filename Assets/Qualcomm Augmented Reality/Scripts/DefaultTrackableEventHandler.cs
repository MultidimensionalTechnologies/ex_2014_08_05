/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Connected Experiences, Inc.
==============================================================================*/

using UnityEngine;
using System.Collections;

/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class DefaultTrackableEventHandler : MonoBehaviour,
                                            ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES
 
    private TrackableBehaviour mTrackableBehaviour;
    
    #endregion // PRIVATE_MEMBER_VARIABLES

	public Transform Position1;
	public Transform Position2;
	private GameObject ArCamera;
	private GameObject AngleContainer;
	private GameObject Book;

    #region UNTIY_MONOBEHAVIOUR_METHODS
    
    void Start()
    {
		ArCamera = GameObject.Find( "ARCamera" );
		AngleContainer = GameObject.Find( "AngleContainer" );
		Book = GameObject.Find ("Book");

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS



    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS

	private void OnTrackingStart()
	{


	}

    private void OnTrackingFound()
    {
		
		if( Vector3.Dot( ArCamera.transform.forward, Position2.forward ) < 0  )
		{
			AngleContainer.transform.localPosition = Position2.localPosition;
			AngleContainer.transform.localRotation = Position2.localRotation;
			//	transform.localScale = Position2.localScale;
		}
		else
		{
			AngleContainer.transform.localPosition = Position1.localPosition;
			AngleContainer.transform.localRotation = Position1.localRotation;
			//	transform.localScale = Position1.localScale;
		}

		Camera.main.GetComponent<S_UserInterface>().Tracked = true;

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		ParticleSystem[] particleComponents = GetComponentsInChildren<ParticleSystem> (true);

        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

		foreach (ParticleSystem component in particleComponents)
		{
			component.Play(true);
		}

		StartCoroutine ("CoLoading");

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
    }

	private IEnumerator CoLoading()
	{
		Book.SetActive (false);
		yield return new WaitForSeconds (6);

		ParticleSystem[] particleComponents = GetComponentsInChildren<ParticleSystem> (true);
		foreach (ParticleSystem component in particleComponents)
		{
			component.Stop(true);
		}

		yield return new WaitForSeconds (3);
		Book.SetActive (true);

	}


    private void OnTrackingLost()
    {
		Camera.main.GetComponent<S_UserInterface>().Tracked = false;

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		ParticleSystem[] particleComponents = GetComponentsInChildren<ParticleSystem> (true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }

		foreach (ParticleSystem component in particleComponents)
		{
			component.Stop(true);
		}

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }

    #endregion // PRIVATE_METHODS
}
